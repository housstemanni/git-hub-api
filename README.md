# GITHUB-API

This repository contains an Android application that allows user to login via Github (two factor authentication possible) and display top Kotlin public repositories since one year ago with paging loading and offline mode support.

## Used language and libraries
- Kotlin - primary project language
- Navigation component - handles all navigation aspects, allows to avoid boilerplate code with fragments transaction, backstack etc.
- Android Architecture Components - the core of MVVM pattern
- Paging library - painless paging loading
- RxJava - efficient way to manage data chains
- Dagger - dependency injection framework
- Retrofit - to perform API call
- Glide - fluid way to load and cache images
- Room - for data persistence used in offline mode
- Reactivenetwork - allows listening to network connection state and Internet connectivity with Reactive Programming approach

## Architecture 
The application is based on MVVM architecture and The project contains the following packages :

- data: contains all the data accessing and manipulating components.
- di: Dependency injection classes, mainly modules and components.
- ui: view classes and their adapters
- viewmodels: view models corresponding to each view 
- utils: Utility classes and helpers
- app : Custom Application class implementation to be adapted to injection 