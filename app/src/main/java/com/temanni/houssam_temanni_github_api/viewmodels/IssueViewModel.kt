package com.temanni.houssam_temanni_github_api.viewmodels

import android.content.Intent
import android.net.Uri
import android.view.View
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.temanni.houssam_temanni_github_api.R
import com.temanni.houssam_temanni_github_api.data.model.Issue

class IssueViewModel : ViewModel() {

    private val ownerName = MutableLiveData<String>()
    fun getOwnerName() = ownerName

    private val title = MutableLiveData<String>()
    fun getTitle() = title

    private val updatedOn = MutableLiveData<String>()
    fun updatedOn() = updatedOn


    private val avatarUrl = MutableLiveData<String>()
    fun getAvatarUrl() = avatarUrl

    private var pullRequestsUrl: String? = null

    fun bind(
        issue: Issue
    ) {
        title.value = issue.title
        ownerName.value = issue.user.name
        avatarUrl.value = issue.user.avatarUrl
        updatedOn.value = issue.updatedAt
        pullRequestsUrl = issue.pullRequest?.htmlUrl
    }

    fun onPullRequestButtonClick(view: View) {
        if (pullRequestsUrl != null) {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(pullRequestsUrl))
            view.context.startActivity(browserIntent)
        } else {
            val toast = Toast.makeText(
                view.context, R.string.no_pr_toast, Toast.LENGTH_SHORT
            )
            toast.show()
        }
    }
}