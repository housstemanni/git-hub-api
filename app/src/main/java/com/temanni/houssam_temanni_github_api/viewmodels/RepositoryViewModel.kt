package com.temanni.houssam_temanni_github_api.viewmodels

import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.temanni.houssam_temanni_github_api.data.model.Repository

class RepositoryViewModel : ViewModel() {
    private lateinit var onItemClick: (Repository) -> Unit
    private lateinit var repository: Repository

    private val avatarUrl = MutableLiveData<String>()
    fun getAvatarUrl() = avatarUrl

    private val fullName = MutableLiveData<String>()
    fun getFullName() = fullName

    private val ownerName = MutableLiveData<String>()
    fun getOwnerName() = ownerName

    private val updateOn = MutableLiveData<String>()
    fun getUpdateOn() = updateOn

    private val stars = MutableLiveData<Int>()
    fun getStars() = stars

    private val forks = MutableLiveData<Int>()
    fun getForks() = forks

    fun bind(
        repository: Repository,
        onItemClick: (Repository) -> Unit
    ) {
        this.repository = repository
        avatarUrl.value = this.repository.owner.avatarUrl
        fullName.value = this.repository.fullName
        ownerName.value = this.repository.owner.name
        updateOn.value = this.repository.updatedOn
        stars.value = this.repository.starts
        forks.value = this.repository.forks
        this.onItemClick = onItemClick
    }

    fun onItemClick(view: View) {
        onItemClick(repository)
    }
}