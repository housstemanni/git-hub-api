package com.temanni.houssam_temanni_github_api.utils

import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.temanni.houssam_temanni_github_api.R
import java.text.SimpleDateFormat

@BindingAdapter("bind:imageUrl")
fun loadImage(view: ImageView, imageUrl: String?) {
    Glide.with(view.context)
        .load(imageUrl)
        .placeholder(
            R.mipmap.ic_place_holder
        )
        .into(view)
}

@BindingAdapter("bind:formattedDate")
fun setFormattedDate(view: TextView, date: String) {
    val parser = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
    val formatter = SimpleDateFormat("dd MMMM yyyy")
    view.text = String.format(
        view.context.getString(R.string.update_on_text_holder),
        formatter.format(parser.parse(date))
    )
}

