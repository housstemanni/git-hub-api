package com.temanni.houssam_temanni_github_api.utils

import android.view.View
import androidx.fragment.app.FragmentActivity
import com.temanni.houssam_temanni_github_api.app.GitHubApplication
import com.temanni.houssam_temanni_github_api.di.component.AppComponent

val FragmentActivity.appComponent: AppComponent get() = (application as GitHubApplication).appComponent
fun View.show() = let { visibility = View.VISIBLE }
fun View.hide() = let { visibility = View.GONE }

