package com.temanni.houssam_temanni_github_api.utils

import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

fun getLastYearDate(): String {
    val calendar = Calendar.getInstance()
    calendar.time = Date()
    calendar.add(Calendar.YEAR, -1)
    val tz: TimeZone = TimeZone.getTimeZone("UTC")
    val df: DateFormat =
        SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'")
    df.timeZone = tz
    return df.format(calendar.time)
}
