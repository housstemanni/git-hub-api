package com.temanni.houssam_temanni_github_api.paging.source

import android.util.Log
import androidx.paging.PageKeyedDataSource
import androidx.paging.PagedList
import com.temanni.houssam_temanni_github_api.data.model.StateError
import com.temanni.houssam_temanni_github_api.data.model.StateLiveData
import com.temanni.houssam_temanni_github_api.data.model.StateLoadSuccess
import com.temanni.houssam_temanni_github_api.data.model.StateProgress
import com.temanni.houssam_temanni_github_api.utils.PAGE_ITEMS_LIMIT
import com.temanni.houssam_temanni_github_api.data.model.Repository
import com.temanni.houssam_temanni_github_api.data.remote.api.GitHubApi
import io.reactivex.disposables.CompositeDisposable
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors


class RepositoriesPageDataSource(
    private val disposable: CompositeDisposable,
    private val state: StateLiveData,
    private val gitHubApi: GitHubApi,
    private val updateDataBaseCallBack: (List<Repository>) -> Unit,
) : PageKeyedDataSource<Int, Repository>() {

    companion object {
        val pageListConfig: PagedList.Config
            get() = PagedList.Config.Builder()
                .setPageSize(PAGE_ITEMS_LIMIT)
                .setEnablePlaceholders(false)
                .build()
        val executor: ExecutorService = Executors.newFixedThreadPool(4)
    }

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, Repository>
    ) {
        val page = 0
        val d = gitHubApi.getRepositories(0, PAGE_ITEMS_LIMIT)
            .doOnSubscribe { state.postValue(StateProgress) }
            .subscribe({
                state.postValue(StateLoadSuccess)
                try {
                    if (it != null) {
                        it.body()?.items?.let { it1 ->
                            updateDataBaseCallBack(it1)
                            callback.onResult(it1, null, page + 1)
                        }
                    }
                } catch (e: IllegalStateException) {
                    Log.e("-HT-", e.localizedMessage)
                }
            }, {
                state.postValue(
                    StateError(
                        it
                    )
                )
            })
        disposable.add(d)
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Repository>) {
        val page = params.key
        val d =
            gitHubApi.getRepositories(page+1, PAGE_ITEMS_LIMIT)
                .doOnSubscribe { state.postValue(StateProgress) }
                .subscribe({
                    state.postValue(StateLoadSuccess)
                    try {
                        if (it != null) {
                            it.body()?.items?.let { it1 ->
                                updateDataBaseCallBack(it1)
                                callback.onResult(it1, page + 1)
                            }
                        }
                    } catch (e: IllegalStateException) {
                        Log.e("-HT-", e.localizedMessage)
                    }
                }, {
                    state.postValue(
                        StateError(
                            it
                        )
                    )
                })
        disposable.add(d)
    }

    override fun loadBefore(
        params: LoadParams<Int>,
        callback: LoadCallback<Int, Repository>
    ) = Unit

}