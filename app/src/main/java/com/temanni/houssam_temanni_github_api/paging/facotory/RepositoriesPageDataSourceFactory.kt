package com.temanni.houssam_temanni_github_api.paging.facotory

import androidx.paging.DataSource
import com.temanni.houssam_temanni_github_api.data.model.StateLiveData
import com.temanni.houssam_temanni_github_api.data.model.Repository
import com.temanni.houssam_temanni_github_api.data.remote.api.GitHubApi
import com.temanni.houssam_temanni_github_api.paging.source.RepositoriesPageDataSource
import io.reactivex.disposables.CompositeDisposable

class RepositoriesPageDataSourceFactory(
    private val disposable: CompositeDisposable,
    private val state: StateLiveData,
    private val gitHubApi: GitHubApi,
    private val updateDataBaseCallBack: (List<Repository>) -> Unit,
) : DataSource.Factory<Int, Repository>() {

    private var pageDataSource: RepositoriesPageDataSource? = null

    override fun create(): DataSource<Int, Repository> {
        val source =
            RepositoriesPageDataSource(
                disposable,
                state,
                gitHubApi,
                updateDataBaseCallBack
            )
        pageDataSource = source
        return source
    }
}