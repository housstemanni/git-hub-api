package com.temanni.houssam_temanni_github_api.data.model

data class RepositoriesResponse(val items: List<Repository>)
