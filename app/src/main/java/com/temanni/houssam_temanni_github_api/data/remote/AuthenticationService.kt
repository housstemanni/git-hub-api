package com.temanni.houssam_temanni_github_api.data.remote

import android.annotation.SuppressLint
import android.content.SharedPreferences
import androidx.annotation.VisibleForTesting
import com.google.gson.Gson
import com.temanni.houssam_temanni_github_api.data.local.GitHubDataBase
import com.temanni.houssam_temanni_github_api.data.model.Authentication
import com.temanni.houssam_temanni_github_api.data.model.User
import com.temanni.houssam_temanni_github_api.data.remote.api.GitHubApi
import com.temanni.houssam_temanni_github_api.utils.ConnectivityHelper
import com.temanni.houssam_temanni_github_api.utils.SHARED_PREFERENCES_CURRENT_AUTHENTICATION_KEY
import com.temanni.houssam_temanni_github_api.utils.SHARED_PREFERENCES_USER_KEY
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

@SuppressLint("CheckResult")
class AuthenticationService @Inject constructor() {

    @Inject
    lateinit var gitHubApi: GitHubApi

    @Inject
    lateinit var sharedPreferences: SharedPreferences

    @Inject
    lateinit var connectivityHelper: ConnectivityHelper

    @Inject
    lateinit var gitHubDataBase: GitHubDataBase

    var gson = Gson()

    // Public functions
    fun getAccessToken(code: String): Single<Authentication?> {
        return gitHubApi.getAccessToken(code).map {
            val authentication = it.body()
            authentication?.let { it1 -> updateAuthentication(it1) }
            authentication
        }
    }

    fun getUserInfo(
        goToHome: (user: User) -> Unit,
        showLoginCallBack: () -> Unit
    ) {
        val currentAuthentication = getCurrentAuthentication()
        if ((currentAuthentication != null)) {
            getAuthenticatedUser(
                currentAuthentication.accessToken,
                currentAuthentication.refreshToken
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ user ->
                    if (user != null) {
                        with(sharedPreferences.edit()) {
                            putString(
                                SHARED_PREFERENCES_USER_KEY,
                                gson
                                    .toJson(user)
                            )
                            apply()
                        }
                        goToHome(user)
                    }
                }, {
                    showLoginCallBack()
                })
        } else {
            showLoginCallBack()
        }
    }

    fun checkLogin(
        goToHomeCallback: (user: User) -> Unit,
        showLoginCallBack: () -> Unit,
        errorCallback: () -> Unit
    ) {
        connectivityHelper.isConnected().firstElement()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ isConnected ->
                if (isConnected) {
                    getUserInfo(goToHomeCallback, showLoginCallBack)
                } else {
                    handleOffLineCase(goToHomeCallback, errorCallback)
                }

            }, {
                errorCallback
            })
    }

    fun logOut(redirectionCallBack: () -> Unit) {
        Completable.fromAction {
            gitHubDataBase.dao().deleteAll()
            sharedPreferences.edit().clear().commit()
        }
            .subscribeOn(Schedulers.io())
            .subscribe {
                redirectionCallBack()
            }
    }

    // private functions
    private fun getAuthenticatedUser(accessToken: String, refreshToken: String): Single<User?> {
        return gitHubApi.getAuthenticatedUser("Bearer $accessToken")
            .flatMap { response ->
                // Handle Authentication errors
                if ((response.code() != 200) && (refreshToken != null)) {
                    // Try to refresh current access token
                    refreshAccessToken(refreshToken).flatMap { auth ->
                        updateAuthentication(auth)
                        getAuthenticatedUser(
                            auth.accessToken, auth.accessToken
                        )
                    }
                } else {
                    Single.just(response.body())
                }
            }
    }

    private fun updateAuthentication(authentication: Authentication) {
        with(sharedPreferences.edit()) {

            putString(
                SHARED_PREFERENCES_CURRENT_AUTHENTICATION_KEY,
                gson
                    .toJson(authentication)
            )

            apply()
        }

    }

    private fun refreshAccessToken(code: String): Single<Authentication?> {
        return gitHubApi.getRefreshedToken(code).map { newAuthentication ->
            if (newAuthentication.code() == 200) {
                newAuthentication.body()
            } else {
                // If refresh access token failed then invoke error to show login web view
                throw RuntimeException("Authentication error")
            }
        }
    }

    private fun handleOffLineCase(
        goToHomeCallback: (user: User) -> Unit,
        errorCallback: () -> Unit
    ) {
        var alreadyConnectedUser =
            gson.fromJson(
                sharedPreferences.getString(
                    SHARED_PREFERENCES_USER_KEY,
                    null
                ), User::class.java
            )
        if (alreadyConnectedUser != null) {
            goToHomeCallback(alreadyConnectedUser)
        } else {
            errorCallback()
        }
    }

    private fun getCurrentAuthentication(): Authentication? {
        return gson.fromJson(
            sharedPreferences.getString(
                SHARED_PREFERENCES_CURRENT_AUTHENTICATION_KEY,
                null
            ), Authentication::class.java
        )
    }

    // Used only for Unit test
    @VisibleForTesting(otherwise = VisibleForTesting.NONE)
    fun initValues(
        sharedPreferences: SharedPreferences,
        gitHubApi: GitHubApi,
        gson: Gson,
        connectivityHelper: ConnectivityHelper
    ) {
        this.sharedPreferences = sharedPreferences
        this.gitHubApi = gitHubApi
        this.gson = gson
        this.connectivityHelper = connectivityHelper
    }
}