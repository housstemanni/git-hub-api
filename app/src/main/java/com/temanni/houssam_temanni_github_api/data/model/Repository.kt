package com.temanni.houssam_temanni_github_api.data.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import com.temanni.houssam_temanni_github_api.data.model.User
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity
data class Repository(
    @PrimaryKey
    val id: Long,
    @SerializedName("full_name") val fullName: String,
    @SerializedName("name") val name: String,
    @SerializedName("forks_count") val forks: Int,
    @SerializedName("watchers") val watchers: Int,
    @SerializedName("stargazers_count") val starts: Int,
    @SerializedName("updated_at") val updatedOn: String,
    @SerializedName("owner") var owner: User
) : Parcelable



