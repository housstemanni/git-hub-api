package com.temanni.houssam_temanni_github_api.data.local

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.temanni.houssam_temanni_github_api.data.model.Repository

class RepositoryConverter {
    @TypeConverter
    fun fromString(value: String?): Repository {
        return Gson().fromJson(value, Repository::class.java)
    }

    @TypeConverter
    fun fromObject(repository: Repository): String {
        val gson = Gson()
        return gson.toJson(repository)
    }
}