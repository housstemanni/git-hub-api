package com.temanni.houssam_temanni_github_api.ui.fragments

import android.os.Bundle
import android.view.*
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.temanni.houssam_temanni_github_api.R
import com.temanni.houssam_temanni_github_api.data.model.Repository
import com.temanni.houssam_temanni_github_api.data.model.StateError
import com.temanni.houssam_temanni_github_api.data.model.StateProgress
import com.temanni.houssam_temanni_github_api.data.model.State
import com.temanni.houssam_temanni_github_api.databinding.FragmentListIssuesBinding
import com.temanni.houssam_temanni_github_api.ui.adapters.IssuesAdapter
import com.temanni.houssam_temanni_github_api.viewmodels.IssuesListViewModel
import com.temanni.houssam_temanni_github_api.utils.ConnectivityHelper
import com.temanni.houssam_temanni_github_api.utils.appComponent
import com.temanni.houssam_temanni_github_api.utils.hide
import com.temanni.houssam_temanni_github_api.utils.show
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_list_repositories.*
import javax.inject.Inject


class IssuesListFragment : Fragment() {

    @Inject
    lateinit var connectivityHelper: ConnectivityHelper

    private lateinit var binding: FragmentListIssuesBinding
    private lateinit var viewModel: IssuesListViewModel
    private lateinit var adapter: IssuesAdapter
    private var selectedRepository: Repository? = null
    private val disposable = CompositeDisposable()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_list_issues, container, false);
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.appComponent?.inject(this)
        viewModel = ViewModelProviders.of(this)
            .get(IssuesListViewModel::class.java)
            .also { activity?.appComponent?.inject(it) }
            .apply {
                state.observe(viewLifecycleOwner, Observer { onStateChanged(it) })
            }
        setHasOptionsMenu(true)
    }

    override fun onStart() {
        super.onStart()
        setSelectedRepositoryInfo()

        setAdapter()
        getIssues()
        observeNetwork()
    }

    override fun onStop() {
        super.onStop()
        disposable.dispose()
    }

    private fun setSelectedRepositoryInfo() {
        selectedRepository =
            arguments?.let { IssuesListFragmentArgs.fromBundle(it).currentRepository }
        binding.reposity = selectedRepository
    }

    private fun getIssues() {
        selectedRepository?.let { repository ->
            viewModel.getIssues(repository)
                ?.observe(viewLifecycleOwner, Observer { pagedList ->
                    adapter.submitList(pagedList)
                })
        }
    }

    private fun setAdapter() {
        val layoutManager = LinearLayoutManager(context)
        layoutManager.isSmoothScrollbarEnabled = true
        binding.apply {
            recyclerView.layoutManager = layoutManager

            adapter =
                IssuesAdapter()
            recyclerView.adapter = adapter
        }
    }

    private fun observeNetwork() {
        val subscription = connectivityHelper.isConnected()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ connected ->
                if (!connected) {
                    showError()
                }
            }, {
                showError()
            })
        disposable.add(subscription)
    }

    private fun onStateChanged(state: State?) {
        updateProgress(state is StateProgress)
        if (state is StateError) showError()
    }

    private fun showError() {
        Snackbar.make(root, R.string.error_message, Snackbar.LENGTH_INDEFINITE)
            .setAction(R.string.retry) {
                getIssues()
            }.show()
    }

    private fun updateProgress(isProcess: Boolean) {
        if (isProcess) progress.show() else progress.hide()
    }

}