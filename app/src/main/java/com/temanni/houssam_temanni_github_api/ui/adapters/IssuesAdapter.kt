package com.temanni.houssam_temanni_github_api.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.temanni.houssam_temanni_github_api.R
import com.temanni.houssam_temanni_github_api.data.model.Issue
import com.temanni.houssam_temanni_github_api.databinding.IssueItemBinding
import com.temanni.houssam_temanni_github_api.viewmodels.IssueViewModel

class IssuesAdapter(
) : PagedListAdapter<Issue, IssuesAdapter.ViewHolder>(
    COMPARATOR
) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: IssueItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.issue_item,
            parent,
            false
        )
        return ViewHolder(
            binding
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position) ?: return
        holder.bind(item)
    }

    class ViewHolder(private val binding: IssueItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(
            item: Issue,
        ) {
            val viewModel =
                IssueViewModel()
            viewModel.bind(item)
            binding.viewModel = viewModel
        }
    }

    companion object {
        val COMPARATOR = object : DiffUtil.ItemCallback<Issue>() {
            override fun areItemsTheSame(
                oldItem: Issue,
                newItem: Issue
            ): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(
                oldItem: Issue,
                newItem: Issue
            ): Boolean {
                return oldItem.id == newItem.id
            }
        }
    }
}