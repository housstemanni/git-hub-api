package com.temanni.houssam_temanni_github_api.ui.fragments

import android.annotation.SuppressLint
import android.content.SharedPreferences
import android.os.Bundle
import android.view.*
import androidx.activity.OnBackPressedCallback
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.temanni.houssam_temanni_github_api.R
import com.temanni.houssam_temanni_github_api.data.model.Repository
import com.temanni.houssam_temanni_github_api.data.model.State
import com.temanni.houssam_temanni_github_api.data.model.StateError
import com.temanni.houssam_temanni_github_api.data.model.StateProgress
import com.temanni.houssam_temanni_github_api.data.remote.AuthenticationService
import com.temanni.houssam_temanni_github_api.databinding.FragmentListRepositoriesBinding
import com.temanni.houssam_temanni_github_api.ui.adapters.RepositoriesAdapter
import com.temanni.houssam_temanni_github_api.utils.*
import com.temanni.houssam_temanni_github_api.viewmodels.RepositoriesListViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_list_repositories.*
import javax.inject.Inject

class RepositoriesListFragment : Fragment() {

    @Inject
    lateinit var connectivityHelper: ConnectivityHelper


    @Inject
    lateinit var sharedPreferences: SharedPreferences

    @Inject
    lateinit var authenticationService: AuthenticationService

    private lateinit var binding: FragmentListRepositoriesBinding
    private lateinit var viewModel: RepositoriesListViewModel
    private lateinit var adapter: RepositoriesAdapter
    private var snackbar: Snackbar? = null
    private val disposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        handleBackButton()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_list_repositories,
            container,
            false
        );
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.appComponent?.inject(this)
        viewModel = ViewModelProviders.of(this)
            .get(RepositoriesListViewModel::class.java)
            .also { activity?.appComponent?.inject(it) }
            .apply {
                state.observe(viewLifecycleOwner, Observer { onStateChanged(it) })
            }
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_logout) {
            logOut()
            return true
        }
        return false
    }

    override fun onStart() {
        super.onStart()
        setUserInfo()
        setAdapter()
        getRepositories()
        observeNetwork()
    }

    override fun onStop() {
        super.onStop()
        disposable.dispose()

    }

    @SuppressLint("ApplySharedPref")
    private fun logOut() {
        authenticationService.logOut {
            val direction =
                RepositoriesListFragmentDirections.actionGoLogin()
            findNavController().navigate(direction)
        }
    }

    private fun getRepositories() {
        viewModel.getRepositories()
            ?.observe(viewLifecycleOwner, Observer {
                adapter.submitList(it)
            })
    }

    private fun setAdapter() {
        val layoutManager = LinearLayoutManager(context)
        layoutManager.isSmoothScrollbarEnabled = true
        binding.apply {
            recyclerView.layoutManager = layoutManager
            adapter =
                RepositoriesAdapter {
                    goToIssues(it)
                }
            recyclerView.adapter = adapter
        }
    }

    private fun setUserInfo() {
        binding.user = arguments?.let {
            RepositoriesListFragmentArgs.fromBundle(
                it
            ).user
        }
    }

    private fun onStateChanged(state: State?) {
        updateProgress(state is StateProgress)
        if (state is StateError) showError()
    }

    private fun updateProgress(isProcess: Boolean) {
        if (isProcess) progress.show() else progress.hide()
    }

    private fun showError() {
        snackbar = Snackbar.make(root, R.string.error_message, Snackbar.LENGTH_INDEFINITE)
            .setAction(R.string.retry) {
                getRepositories()
            }
        snackbar?.show()
    }

    private fun observeNetwork() {
        val subscription = connectivityHelper.isConnected()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ connected ->
                if (!connected) {
                    showError()
                }
            }, {
                showError()
            })
        disposable.add(subscription)
    }

    private fun goToIssues(repository: Repository) {
        snackbar?.dismiss()
        val direction =
            RepositoriesListFragmentDirections.actionGoIssues(
                repository
            )
        findNavController().navigate(direction)
    }

    private fun handleBackButton() {
        val callback =
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    activity?.finish()

                }
            }
        requireActivity().onBackPressedDispatcher.addCallback(this, callback)
    }
}