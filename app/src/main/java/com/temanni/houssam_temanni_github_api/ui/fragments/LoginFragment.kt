package com.temanni.houssam_temanni_github_api.ui.fragments

import android.annotation.SuppressLint
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import com.temanni.houssam_temanni_github_api.R
import com.temanni.houssam_temanni_github_api.data.model.User
import com.temanni.houssam_temanni_github_api.data.remote.AuthenticationService
import com.temanni.houssam_temanni_github_api.databinding.FragmentLoginBinding
import com.temanni.houssam_temanni_github_api.utils.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_login.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class LoginFragment : Fragment() {
    @Inject
    lateinit var authenticationService: AuthenticationService

    @Inject
    lateinit var connectivityHelper: ConnectivityHelper

    private lateinit var binding: FragmentLoginBinding;
    private val disposable = CompositeDisposable()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.appComponent?.inject(this)
        checkLogin()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (disposable != null && !disposable.isDisposed) {
            disposable.dispose();
        }
    }

    private fun checkLogin() {
        authenticationService.checkLogin(
            goToHomeCallback = ::goToHome,
            showLoginCallBack = ::showLoginWebView,
            errorCallback = ::showError
        )
    }

    private fun goToHome(user: User) {
        val direction =
            LoginFragmentDirections.actionGoToHome(
                user
            )
        findNavController().navigate(direction)
    }

    @SuppressLint("SetJavaScriptEnabled")
    fun showLoginWebView() {
        val state = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis())
        val url =
            "$AUTHURL?client_id=$CLIENT_ID&scope=$SCOPE&redirect_uri=$REDIRECT_URI&state=$state"
        context?.let { CookiesHelper.clear(it) }
        val webView = binding.webView
        webView.isVerticalScrollBarEnabled = false
        webView.isHorizontalScrollBarEnabled = false
        webView.webViewClient = GithubWebViewClient()
        webView.settings.javaScriptEnabled = true
        webView.loadUrl(url)
    }

    private fun showError() {
        web_view.reload()
        Snackbar.make(binding.root, getString(R.string.error_message), Snackbar.LENGTH_INDEFINITE)
            .setAction(R.string.retry) { checkLogin() }
            .show()
    }

    inner class GithubWebViewClient : WebViewClient() {
        override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
            if (url.startsWith(REDIRECT_URI)) {
                handleUrl(url)
                return true
            }
            return false
        }

        private fun handleUrl(url: String) {
            val uri = Uri.parse(url)
            if (url.contains("code")) {
                val code = uri.getQueryParameter("code") ?: ""
                val subscription =
                    authenticationService.getAccessToken(code).subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({ authentication ->
                            authenticationService.getUserInfo(::goToHome, ::showLoginWebView)
                        }, {
                        })
                disposable.add(subscription)
            }
        }
    }
}