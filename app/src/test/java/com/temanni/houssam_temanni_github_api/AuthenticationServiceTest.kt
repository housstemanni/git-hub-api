package com.temanni.houssam_temanni_github_api

import android.content.SharedPreferences
import com.google.gson.Gson
import com.temanni.houssam_temanni_github_api.data.model.Authentication
import com.temanni.houssam_temanni_github_api.data.model.User
import com.temanni.houssam_temanni_github_api.data.remote.AuthenticationService
import com.temanni.houssam_temanni_github_api.data.remote.api.GitHubApi
import com.temanni.houssam_temanni_github_api.utils.ConnectivityHelper
import com.temanni.houssam_temanni_github_api.utils.SHARED_PREFERENCES_CURRENT_AUTHENTICATION_KEY
import com.temanni.houssam_temanni_github_api.utils.SHARED_PREFERENCES_USER_KEY
import io.mockk.*
import io.mockk.impl.annotations.MockK
import io.mockk.impl.annotations.RelaxedMockK
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Test
import retrofit2.Response

class AuthenticationServiceTest {

    @RelaxedMockK
    private lateinit var sharedPreferences: SharedPreferences

    @RelaxedMockK
    private lateinit var gson: Gson

    @MockK
    private lateinit var gitHubApi: GitHubApi

    @MockK
    private lateinit var connectivityHelper: ConnectivityHelper

    @RelaxedMockK
    lateinit var goToHomeCallback: (User) -> Unit

    @RelaxedMockK
    lateinit var showLoginCallBack: () -> Unit

    @RelaxedMockK
    lateinit var errorCallBack: () -> Unit

    lateinit var fakeUser: User
    lateinit var fakeAuthentication: Authentication
    lateinit var authenticationService: AuthenticationService

    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxUnitFun = true)
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setErrorHandler { t -> println("error = " + t.message) }
        authenticationService = AuthenticationService()
        authenticationService.initValues(sharedPreferences, gitHubApi, gson, connectivityHelper)
        fakeUser = User(1, "user1", "https://github.com/user1")
        fakeAuthentication = Authentication("non_null_access_token", "non_null_refresh_token")
    }

    @Test
    fun `should save fetched user to shared preferences and go to home screen when network is available and user is already logged in`() {
        val authenticationJson = gson.toJson(fakeAuthentication)
        every { connectivityHelper.isConnected() } returns Observable.just(true)
        every { gitHubApi.getAuthenticatedUser(any()) } returns Single.just(
            Response.success(
                fakeUser
            )
        )
        every {
            sharedPreferences.getString(
                SHARED_PREFERENCES_CURRENT_AUTHENTICATION_KEY,
                null
            )
        } returns authenticationJson
        every {
            gson.fromJson(
                authenticationJson,
                Authentication::class.java
            )
        } returns fakeAuthentication
        authenticationService.checkLogin(goToHomeCallback, showLoginCallBack, errorCallBack)

        verify(exactly = 1) {
            sharedPreferences.edit().putString(SHARED_PREFERENCES_USER_KEY, gson.toJson(fakeUser))
        }
        verify(exactly = 1) { goToHomeCallback(fakeUser) }
    }

    @Test
    fun `should show login webView when network is available and no user logged in yet`() {
        val authenticationJson = gson.toJson(null)
        every { connectivityHelper.isConnected() } returns Observable.just(true)
        every { gitHubApi.getAuthenticatedUser(any()) } returns Single.just(
            Response.success(
                fakeUser
            )
        )

        every {
            sharedPreferences.getString(
                SHARED_PREFERENCES_CURRENT_AUTHENTICATION_KEY,
                null
            )
        } returns authenticationJson
        every { gson.fromJson(authenticationJson, Authentication::class.java) } returns null
        authenticationService.checkLogin(goToHomeCallback, showLoginCallBack, errorCallBack)
        verify(exactly = 1) { showLoginCallBack() }
    }

    @Test
    fun `should go to home screen when network is not available and user is already logged in`() {
        val userJson = gson.toJson(fakeUser)
        every { connectivityHelper.isConnected() } returns Observable.just(false)
        every {
            sharedPreferences.getString(
                SHARED_PREFERENCES_USER_KEY,
                null
            )
        } returns userJson
        every { gson.fromJson(userJson, User::class.java) } returns fakeUser
        authenticationService.checkLogin(goToHomeCallback, showLoginCallBack, errorCallBack)
        verify(exactly = 1) { goToHomeCallback(fakeUser) }
    }

    @Test
    fun `should show error when network is not available and no user logged in yet`() {
        val userJson = gson.toJson(null)
        every { connectivityHelper.isConnected() } returns Observable.just(false)

        every {
            sharedPreferences.getString(
                SHARED_PREFERENCES_USER_KEY,
                null
            )
        } returns userJson
        every { gson.fromJson(userJson, User::class.java) } returns null
        authenticationService.checkLogin(goToHomeCallback, showLoginCallBack, errorCallBack)
        verify(exactly = 1) { errorCallBack() }
    }

}

